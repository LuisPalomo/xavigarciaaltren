import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {CitiesModel} from '../models/cities-model';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ApiService {

  public apiUrl: string;
  public apiKey: string;
  public city1: CitiesModel;
  public city2: CitiesModel;
  public city3: CitiesModel;
  public city4: CitiesModel;

  constructor(private _http: HttpClient) {
    this.apiUrl = 'http://api.openweathermap.org/data/2.5/weather?q=';
    this.apiKey = '51092dfe3b022538aa11b1df7c90658c';
  }

  // return data from all cities
  getPostsType2(cities: any[]) {
    const calls = [];
    cities.forEach((value) => {
      calls.push(this._http.get(this.apiUrl + value + '&appid=' + this.apiKey));
    });
    return Observable.forkJoin(calls);
  }
}

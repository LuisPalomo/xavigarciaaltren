import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {CitiesModel} from '../models/cities-model';

declare var Materialize: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ApiService]
})
export class HomeComponent implements OnInit {

  public cities: any[];
  public city1: CitiesModel;
  public city2: CitiesModel;
  public city3: CitiesModel;
  public city4: CitiesModel;
  public newCity: string;
  public getHist: any[];
  public historyExist: boolean;

  constructor(private _postService: ApiService) {

    this.city1 = new CitiesModel('Santiago', {humidity: 0, temp: 0, temp_max: 0, temp_min: 0, pressure: 0});
    this.city2 = new CitiesModel('Buenos Aires', {humidity: 0, temp: 0, temp_max: 10, temp_min: 0, pressure: 0});
    this.city3 = new CitiesModel('Lima', {humidity: 0, temp: 0, temp_max: 0, temp_min: 10, pressure: 0});
    this.city4 = new CitiesModel('Sao Paulo', {humidity: 0, temp: 0, temp_max: 0, temp_min: 0, pressure: 0});
    this.newCity = '';
    this.cities = [this.city1, this.city2, this.city3, this.city4];
    this.getHist = [];
    this.historyExist = false;
    setInterval(() => {
      this.getCitiesData();
    }, 1000 * 60 * 3);
  }

  ngOnInit() {
    this.getCitiesData();
    this.getHistory();
  }

  getCitiesData( newCity = null) {
    const cities = [];
    for (const city of this.cities) {
      cities.push(city.name);
    }
    if (newCity) {
      cities.push(newCity);
    }
    this.getCityData(cities);
  }

  getCityData(city: string[]) {
    this._postService.getPostsType2(city)
      .subscribe(
        result => {
          this.cities = result;
        },
        error => {
          console.log(error);
          Materialize.toast('Error obteniendo datos de ciudad: ' + error.statusText, 4000, 'red');
        },
        () => {
          const date = new Date();
          const strDate = date.toString();
          // grabo en localStorage la última
          localStorage.setItem('last', JSON.stringify(this.cities));
          // grabo historial con fecha y hora
          localStorage.setItem(strDate, JSON.stringify(this.cities));
        }
      );
  }

  getHistory() {
    if (localStorage.length > 1) {
      this.historyExist = true;
      for (let i = 0; i < localStorage.length; i++) {
        if (localStorage.key(i) !== 'last') {
          this.getHist.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
        }
      }
      this.getHist.reverse();
    }
  }

  onSubmit() {
   this.getCitiesData(this.newCity);
  }
}
